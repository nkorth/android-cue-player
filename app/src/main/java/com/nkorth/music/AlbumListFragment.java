package com.nkorth.music;

import android.app.ListFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AlbumListFragment extends ListFragment {
    private AlbumAdapter adapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setEmptyText(getActivity().getString(R.string.no_albums));
        loadAlbums();
    }

    public void loadAlbums(){
        if(adapter != null) {
            setListShownNoAnimation(false);
        }
        LoadLibraryTask task = new LoadLibraryTask(){
            @Override
            protected void onPostExecute(List<Album> albums) {
                adapter = new AlbumAdapter(albums);
                setListAdapter(adapter);
                setListShown(true);
            }
        };
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if(sp.getBoolean(SettingsActivity.KEY_USE_EXTERNAL, false)){
            task.execute(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC),
                    new File(SettingsActivity.getExternalStorageDir()+"/Music"));
        } else {
            task.execute(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC));
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Album album = adapter.getItem(position);
        Intent i = new Intent(PlaybackService.ACTION_PLAY, album.cuesheet, getActivity(), PlaybackService.class);
        getActivity().startService(i);
    }

    private class AlbumAdapter implements ListAdapter {
        private List<Album> albums;

        public AlbumAdapter(List<Album> albums){
            this.albums = new ArrayList<>(albums);
        }

        @Override
        public int getCount() {
            return albums.size();
        }

        @Override
        public boolean isEmpty() {
            return albums.isEmpty();
        }

        @Override
        public Album getItem(int position) {
            return albums.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v;
            Album album = albums.get(position);
            if(convertView != null) {
                v = convertView;
            } else{
                LayoutInflater inflater = getActivity().getLayoutInflater();
                v = inflater.inflate(R.layout.album_list_item, parent, false);
            }
            TextView title = (TextView) v.findViewById(R.id.title);
            title.setText(album.title);
            TextView performer = (TextView) v.findViewById(R.id.performer);
            performer.setText(album.performer);
            ImageView cover = (ImageView) v.findViewById(R.id.cover);
            cover.setImageBitmap(album.cover);
            return v;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {
            // do nothing, since dataset won't change
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
            // do nothing, since dataset won't change
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }
    }
}
