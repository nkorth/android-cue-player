package com.nkorth.music;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadata;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Album {
    public Uri cuesheet;
    public Uri audio;
    public String title;
    public String performer;
    public Bitmap cover;
    public List<Track> tracks;

    public class Track {
        public String title;
        public String performer;
        public int position;
        public int pregap;
    }

    public Album(File cuesheetFile) throws IOException {
        cuesheet = Uri.fromFile(cuesheetFile);
        tracks = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new FileReader(cuesheetFile))) {
            String line;
            Track currentTrack = null;
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                if (line.startsWith("FILE ") && audio == null) {
                    String audioFilename = removeQuotes(line.substring(5, line.lastIndexOf('"')+1).trim());
                    File audioFile = new File(cuesheetFile.getParent(), audioFilename);
                    if(!audioFile.isFile()){
                        throw new IOException("Audio file does not exist "+audioFile.getPath());
                    }
                    audio = Uri.fromFile(audioFile);
                } else if (line.startsWith("TRACK ")) {
                    if(currentTrack != null){
                        tracks.add(currentTrack);
                    }
                    currentTrack = new Track();
                } else if(currentTrack == null) {
                    // album properties
                    if(line.startsWith("TITLE ")){
                        title = removeQuotes(line.substring(6).trim());
                    } else if(line.startsWith("PERFORMER ")){
                        performer = removeQuotes(line.substring(10).trim());
                    }
                } else {
                    // track properties
                    if(line.startsWith("TITLE ")){
                        currentTrack.title = removeQuotes(line.substring(6).trim());
                    } else if(line.startsWith("PERFORMER ")){
                        currentTrack.performer = removeQuotes(line.substring(10).trim());
                    } else if(line.startsWith("INDEX 01 ")){
                        currentTrack.position = parseTime(line.substring(9).trim());
                    } else if(line.startsWith("INDEX 00 ")){
                        currentTrack.pregap = parseTime(line.substring(9).trim());
                    }
                }
            }
            if(currentTrack != null){
                tracks.add(currentTrack);
            }
        }
        if(audio == null || title == null || performer == null){
            throw new IOException("Invalid cuesheet file");
        }
        File cover1 = new File(cuesheetFile.getPath().substring(cuesheetFile.getPath().length()-4)+".jpg");
        File cover2 = new File(cuesheetFile.getParent(), "folder.jpg");
        if(cover1.isFile()){
            cover = BitmapFactory.decodeFile(cover1.getPath());
        } else if(cover2.isFile()){
            cover = BitmapFactory.decodeFile(cover2.getPath());
        }
    }

    public int findCurrentTrack(int currentTime){
        for(int track = 1; track < tracks.size(); track++){
            if(currentTime < tracks.get(track).position){
                Log.w("track", "track "+(track - 1));
                return track - 1;
            }
        }
        Log.w("track", "track "+(tracks.size()-1));
        return tracks.size()-1;
    }

    public MediaMetadata getMetadata(int trackNumber){
        return new MediaMetadata.Builder()
                .putBitmap(MediaMetadata.METADATA_KEY_ALBUM_ART, cover)
                .putString(MediaMetadata.METADATA_KEY_ALBUM, title)
                .putString(MediaMetadata.METADATA_KEY_ALBUM_ARTIST, performer)
                .putLong(MediaMetadata.METADATA_KEY_TRACK_NUMBER, trackNumber)
                .putLong(MediaMetadata.METADATA_KEY_NUM_TRACKS, tracks.size())
                .putString(MediaMetadata.METADATA_KEY_TITLE, tracks.get(trackNumber).title)
                .putString(MediaMetadata.METADATA_KEY_ARTIST, tracks.get(trackNumber).performer)
                .build();
    }

    private String removeQuotes(String str){
        if(str.startsWith("\"") && str.endsWith("\"")){
            return str.substring(1, str.length()-1);
        }
        return str;
    }

    private int parseTime(String str){
        int minutes = Integer.parseInt(str.substring(0, 2));
        int seconds = Integer.parseInt(str.substring(3, 5));
        int frames = Integer.parseInt(str.substring(6, 8));
        return (60 * 1000 * minutes) + (1000 * seconds) + (13 * frames);
    }
}
